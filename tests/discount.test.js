const mongoose = require('mongoose')
const { app } = require('../src/app')
const request = require('supertest')
const { nanoid } = require('nanoid')
const Discount = require('../src/models/discount.model')


const name = nanoid(4)


beforeAll(async () => {
    await mongoose.connect(process.env.MONGO_URL)
})


describe('Create new discount mehtod:post endpoint: /discount/create', () => {
    it('when everything is ok!', async () => {
        const response = await request(app)
            .post('/discount/create')
            .send({
                name: name,
                description: "desc",
                discount_percent: 10
            })
            .expect(200)
            .expect('Content-type', /json/);
        expect(response.body).toStrictEqual({
            msg: 'discount create successfully'
        })
    })

    it('when name is empty', async () => {
        const response = await request(app)
            .post('/discount/create')
            .send({
                name: '',
                description: "desc",
                discount_percent: 10
            })
            .expect(400)
            .expect('Content-type', /json/);
        expect(response.body).toStrictEqual({
            errors: ['name is empty!']
        })
    })

    it('when description is empty', async () => {
        const response = await request(app)
            .post('/discount/create')
            .send({
                name: nanoid(4),
                description: '',
                discount_percent: 10
            })
            .expect(400)
            .expect('Content-type', /json/);
        expect(response.body).toStrictEqual({
            errors: ['description is empty!']
        })
    })

    it('when id is empty', async () => {
        const response = await request(app)
            .post('/discount/create')
            .send({
                name: nanoid(4),
                description: 'desc',
            })
            .expect(400)
            .expect('Content-type', /json/);
        expect(response.body).toStrictEqual({
            errors: ['discount_percent is empty!']
        })
    })

    it('when discount_percent is not a number', async () => {
        const response = await request(app)
            .post('/discount/create')
            .send({
                name: nanoid(4),
                description: 'desc',
                discount_percent: 'not a number'
            })
            .expect(400)
            .expect('Content-type', /json/);
        expect(response.body).toStrictEqual({
            errors: ['discount_percent is not a number!']
        })
    })

    it('when discount_percent is not between 0 and 100', async () => {
        const response = await request(app)
            .post('/discount/create')
            .send({
                name: nanoid(4),
                description: 'desc',
                discount_percent: 1000
            })
            .expect(400)
            .expect('Content-type', /json/);
        expect(response.body).toStrictEqual({
            errors: ['discount_percent is not between 0 and 100!']
        })
    })

    it('when name is previuosly used for another discount', async () => {
        const response = await request(app)
            .post('/discount/create')
            .send({
                name: name,
                description: 'desc',
                discount_percent: 10
            })
            .expect(400)
            .expect('Content-type', /json/);
        expect(response.body).toStrictEqual({
            errors: [`'${name}' is already used for another Dsicount`]
        })
    })
})

describe('showing discounts mehtod:get endpoint: /discount/show', () => {

    it('must show discounts', async () => {
        const response = await request(app)
            .get('/discount/show')
            .expect(200)
            .expect('Content-Type', /json/);
    })
})

describe('Update a discount mehtod:put endpoint: /discount/update', () => {

    it('when everything is ok!', async () => {
        const response = await request(app)
            .put('/discount/update')
            .send({
                id: '6297389c22436ea673793c12',
                name: 'alimamad',
                description: "desc",
                discount_percent: 10
            })
            .expect(200)
            .expect('Content-type', /json/);
        expect(response.body).toStrictEqual({
            msg: 'discount updated successfully'
        })
    })

    it('when name is empty', async () => {
        const response = await request(app)
            .put('/discount/update')
            .send({
                id: '6297389c22436ea673793c12',
                name: '',
                description: "desc",
                discount_percent: 10
            })
            .expect(400)
            .expect('Content-type', /json/);
        expect(response.body).toStrictEqual({
            errors: ['name is empty!']
        })
    })

    it('when description is empty', async () => {
        const response = await request(app)
            .put('/discount/update')
            .send({
                id: '6297389c22436ea673793c12',
                name: 'alimamad',
                description: '',
                discount_percent: 10
            })
            .expect(400)
            .expect('Content-type', /json/);
        expect(response.body).toStrictEqual({
            errors: ['description is empty!']
        })
    })

    it('when discount_percent is empty', async () => {
        const response = await request(app)
            .put('/discount/update')
            .send({
                id: '6297389c22436ea673793c12',
                name: 'alimamad',
                description: 'desc',
            })
            .expect(400)
            .expect('Content-type', /json/);
        expect(response.body).toStrictEqual({
            errors: ['discount_percent is empty!']
        })
    })

    it('when id is empty', async () => {
        const response = await request(app)
            .put('/discount/update')
            .send({
                name: 'alimamad',
                description: "desc",
                discount_percent: 10
            })
            .expect(400)
            .expect('Content-type', /json/);
        expect(response.body).toStrictEqual({
            errors: ['id is empty!', "'alimamad' is already used for another Dsicount"]
        })
    })

    it('when discount_percent is not a number', async () => {
        const response = await request(app)
            .put('/discount/update')
            .send({
                id: '6297389c22436ea673793c12',
                name: 'alimamad',
                description: 'desc',
                discount_percent: 'not a number'
            })
            .expect(400)
            .expect('Content-type', /json/);
        expect(response.body).toStrictEqual({
            errors: ['discount_percent is not a number!']
        })
    })

    it('when discount_percent is not between 0 and 100', async () => {
        const response = await request(app)
            .put('/discount/update')
            .send({
                id: '6297389c22436ea673793c12',
                name: 'alimamad',
                description: 'desc',
                discount_percent: 1000
            })
            .expect(400)
            .expect('Content-type', /json/);
        expect(response.body).toStrictEqual({
            errors: ['discount_percent is not between 0 and 100!']
        })
    })

    it('when name is previuosly used for another discount', async () => {
        const response = await request(app)
            .put('/discount/update')
            .send({
                id: '629748733298f66c5707e2ec',
                name: 'alimamad',
                description: 'desc',
                discount_percent: 10
            })
            .expect(400)
            .expect('Content-type', /json/);
        expect(response.body).toStrictEqual({
            errors: [`'alimamad' is already used for another Dsicount`]
        })
    })
})

describe('Delete a discount mehtod:delete endpoint: /discount/delete', () => {

    it('when everything is ok!', async () => {
        const response = await request(app)
            .delete('/discount/delete')
            .send({
                name: name,
            })
            .expect(200)
            .expect('Content-type', /json/);
        expect(response.body).toStrictEqual({
            msg: 'discount deleted successfully'
        })
    })

    it('when name is empty', async () => {
        const response = await request(app)
            .delete('/discount/delete')
            .send({
                name: '',
            })
            .expect(400)
            .expect('Content-type', /json/);
        expect(response.body).toStrictEqual({
            errors: ['name is empty!']
        })
    })

    it('when name is not found', async () => {
        const response = await request(app)
            .delete('/discount/delete')
            .send({
                name: name,
            })
            .expect(404)
            .expect('Content-type', /json/);
        expect(response.body).toStrictEqual({
            error: `'${name}' is not found`
        })
    })
})


describe('change a discount active flag mehtod:post endpoint: /discount/active', () => {

    it('when everything is ok!', async () => {
        const response = await request(app)
            .post('/discount/changestatus')
            .send({
                name: 'test',
            })
            .expect(200)
            .expect('Content-type', /json/);
    })

    it('when name is empty', async () => {
        const response = await request(app)
            .post('/discount/changestatus')
            .send({
                name: '',
            })
            .expect(400)
            .expect('Content-type', /json/);
        expect(response.body).toStrictEqual({
            errors: ['name is empty!']
        })
    })

    it('when name is not found', async () => {
        const response = await request(app)
            .post('/discount/changestatus')
            .send({
                name: 'notfound',
            })
            .expect(404)
            .expect('Content-type', /json/);
        expect(response.body).toStrictEqual({
            error: `'notfound' is not found`
        })
    })
})

afterAll(async () => {
    await mongoose.disconnect()
})