//Tests for index api
const { app } = require('../src/app');
const request = require('supertest');

describe('GET /', () => {
    it('responds with json', async () => {
        const response = await request(app)
            .get('/');
        expect(response.status).toEqual(200);
        expect(response.body).toEqual({ 'msg': 'welcome to Index!' });
    });
});