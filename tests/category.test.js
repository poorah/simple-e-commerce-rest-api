const { app } = require('../src/app');
const request = require('supertest');
const mongoose = require('mongoose');
const { nanoid } = require('nanoid')


const name = nanoid(4)

beforeAll(async () => {
    await mongoose.connect(process.env.MONGO_URL);
});

describe('Create new category mehtod:post endpoint: /product-catgeory/create', () => {

    it('when every thing is ok!', async () => {
        const response = await request(app)
            .post('/product-category/create')
            .send({ name: name, description: 'test' })
            .expect('Content-Type', /json/)
            .expect(200);

        expect(response.body).toStrictEqual({
            msg: 'category created successfull!'
        });
    })

    it('when name or decription is empty', async () => {
        const response = await request(app)
            .post('/product-category/create')
            .send({})
            .expect('Content-Type', /json/)
            .expect(400);

        expect(response.body).toStrictEqual({
            error: 'name and description field is required!'
        });
    })

    it('when a category whit this name already exists', async () => {
        const response = await request(app)
            .post('/product-category/create')
            .send({ name: 'test', description: 'jfnjf' })
            .expect('Content-Type', /json/)
            .expect(400);

        expect(response.body).toStrictEqual({
            error: 'category with this name already exists!'
        });
    })
})

describe('showing categories mehtod:get endpoint: /product-catgeory/show', () => {

    it('must show categories', async () => {
        const response = await request(app)
            .get('/product-category/show')
            .expect(200)
            .expect('Content-Type', /json/);
    })
})

describe('updaing categories mehtod:put endpoint: /product-catgeory/update', () => {

    it('when every thing is ok!', async () => {
        const response = await request(app)
            .put('/product-category/update')
            .expect(200)
            .send({
                id: "62951cc308f63ca773e97df4",
                name: "akbar",
                description: "change!"
            })
            .expect('Content-Type', /json/);

        expect(response.body).toStrictEqual({
            msg: 'updated successfully'
        })

    })

    it('when name is already exists', async () => {

        const response = await request(app)
            .put('/product-category/update')
            .expect(400)
            .send({
                id: "62951cc308f63ca773e97df4",
                name: "change",
                description: "change!"
            })
            .expect('Content-Type', /json/);

        expect(response.body).toStrictEqual({
            error: 'category with this name already exists!'
        })
    })

    it('when name or description is empty', async () => {

        const response = await request(app)
            .put('/product-category/update')
            .expect(400)
            .send({})
            .expect('Content-Type', /json/);

        expect(response.body).toStrictEqual({
            error: 'name and description field is required!'
        })
    })
})


describe('delete a category mehtod:delete endpoint: /product-catgeory/delete', () => {

    it('when every thing is ok!', async () => {

        const response = await request(app)
            .delete('/product-category/delete')
            .send({ name: name })
            .expect(200)
            .expect('Content-type', /json/);

        expect(response.body).toStrictEqual({
            msg: 'Category deleted successfully!'
        })
    })

    it('when name is not found', async () => {

        const response = await request(app)
            .delete('/product-category/delete')
            .send({ name: nanoid(4) })
            .expect(400)
            .expect('Content-type', /json/);

        expect(response.body).toStrictEqual({
            error: 'there is no category with this name'
        })
    })


    it('when name is empty', async () => {

        const response = await request(app)
            .delete('/product-category/delete')
            .send({})
            .expect(400)
            .expect('Content-type', /json/);

        expect(response.body).toStrictEqual({
            error: 'name is empty!'
        })
    })
})

afterAll(async () => {
    await mongoose.disconnect()
})