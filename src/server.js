require('dotenv').config();
const http = require('http');
const { app } = require('./app.js');
const { connectToMongoDB } = require('./configs/mongodb.config.js')

const PORT = process.env.PORT;

const server = http.createServer(app);

//start server function
async function startServer() {

    //connecting to MongoDB and handling errors
    await connectToMongoDB();

    //running server
    server.listen(PORT, () => {
        console.log(`listening on port ${PORT}`);
    });
}

startServer();