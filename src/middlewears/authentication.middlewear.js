const jwt = require('jsonwebtoken');


//auth middlewear
function authenticateToken(req, res, next) {

  const authHeader = req.headers['authorization'];

  //using && operator to check if token is null or not
  const token = authHeader && authHeader.split(' ')[1];

  // 401 status code for unauthorized
  if (token == null)
    return res.status(401).json({ error: 'unauthorized' });

  // if token was not empty :
  const decoded = jwt.verify(token,
    process.env.JWT_KEY,
    (err, user) => { // user = decoded
      //handling errors:
      if (err) {
        console.log(err);
        // 403 means server understands the request but refuses to authorize it!
        return res.status(403).json({ error: 'forbidden' });
      }

      //assigning decoded value to user
      req.user = user;
      next();
    });
}


module.exports = {
  authenticateToken,
};
