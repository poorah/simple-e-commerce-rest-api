const Discount = require('../models/discount.model')
const { getPagination } = require('../services/query')

//TODO: add this two functions in middlewear

//checking Empty field function
function isEmpty(field, fieldName) {
    if (!field) {
        return `${fieldName} is empty!`
    }
}

//checking field unicity
async function isUnique(fname) {
    //serach for a discount with this name
    const existDiscount = await Discount.findOne({
        name: fname
    }, {
        name: 1
    })
    //console.log(existDiscount);
    if (existDiscount != null && fname != undefined) {
        return `'${fname}' is already used for another Dsicount`
    }
}


// /dicount/create method:POST
async function createDiscount(req, res) {
    try {
        const errors = []
        var error

        //check if name is empty
        error = await isEmpty(req.body.name, 'name')
        if (error != undefined) {
            errors.push(error)
        }

        //check if description is empty
        error = await isEmpty(req.body.description, 'description')
        if (error != undefined) {
            errors.push(error)
        }

        //check if a discount_percent is empty
        error = await isEmpty(req.body.discount_percent, 'discount_percent')
        if (error != undefined) {
            errors.push(error)
        }

        //check if discount_percent is not a number
        if (isNaN(req.body.discount_percent) && req.body.discount_percent) {
            errors.push('discount_percent is not a number!')
        }

        //check if discount_percent is between 0 and 100
        if (req.body.discount_percent < 0 || req.body.discount_percent > 100) {
            errors.push('discount_percent is not between 0 and 100!')
        }

        //check if a discount with this name is already exists
        error = await isUnique(req.body.name)
        if (error != undefined) {
            errors.push(error)
        }

        //handling errors
        if (errors.length != 0) {
            return res.status(400).json({
                errors: errors
            })
        }

        //if everything was ok:
        await Discount.create({
            name: req.body.name,
            description: req.body.description,
            discount_percent: req.body.discount_percent,
        })
        return res.status(200).json({
            msg: 'discount create successfully'
        })
    }
    catch (e) {
        console.log(e)
        return res.status(400).json({
            error: 'an error occured'
        })
    }
}


// /discount/show?page=num&limit=num method:get
async function showDiscounts(req, res) {
    try {
        const { limit, skip } = getPagination(req.query)

        const discounts = await Discount.find({}, {
            _id: 0,
            __v: 0,
        })
            .skip(skip)
            .limit(limit)
            .sort({
                created_at: -1
            })

        return res.status(200).json(discounts)
    }
    catch (e) {
        console.log(e)
        return res.status(400).json({
            error: 'an error occured'
        })
    }

}


// /discount/update method:put
async function updateDiscount(req, res) {
    try {
        const errors = []
        var error

        //check if name is empty
        error = await isEmpty(req.body.name, 'name')
        if (error != undefined) {
            errors.push(error)
        }

        //check if id is empty
        error = await isEmpty(req.body.id, 'id')
        if (error != undefined) {
            errors.push(error)
        }

        //check if description is empty
        error = await isEmpty(req.body.description, 'description')
        if (error != undefined) {
            errors.push(error)
        }

        //check if a discount_percent is empty
        error = await isEmpty(req.body.discount_percent, 'discount_percent')
        if (error != undefined) {
            errors.push(error)
        }

        //check if discount_percent is not a number
        if (isNaN(req.body.discount_percent) && req.body.discount_percent) {
            errors.push('discount_percent is not a number!')
        }

        //check if discount_percent is between 0 and 100
        if (req.body.discount_percent < 0 || req.body.discount_percent > 100) {
            errors.push('discount_percent is not between 0 and 100!')
        }

        //check if a discount with this name is already exists
        const existDiscount = await Discount.findOne({ name: req.body.name })
        if (existDiscount != null && existDiscount.id != req.body.id) {
            errors.push(`'${req.body.name}' is already used for another Dsicount`)
        }

        //handling errors
        if (errors.length != 0) {
            return res.status(400).json({
                errors: errors
            })
        }

        //if everything was ok:
        await Discount.updateOne({
            _id: req.body.id
        }, {
            name: req.body.name,
            description: req.body.description,
            discount_percent: req.body.discount_percent,
            modified_at: Date.now()
        })
        return res.status(200).json({
            msg: 'discount updated successfully'
        })
    }
    catch (e) {
        console.log(e)
        return res.status(400).json({
            error: 'an error occured'
        })
    }
}

// /discount/delete method:delete
async function deleteDiscount(req, res) {
    try {
        const errors = []
        var error

        //check if name is empty
        error = await isEmpty(req.body.name, 'name')
        if (error != undefined) {
            errors.push(error)
        }

        //handling errors
        if (errors.length != 0) {
            return res.status(400).json({
                errors: errors
            })
        }

        //check if a discount with this name is not exists
        const existDiscount = await Discount.findOne({ name: req.body.name })
        if (existDiscount == null) {
            return res.status(404).json({
                error: `'${req.body.name}' is not found`
            })
        }

        //if everything was ok
        await Discount.deleteOne({
            name: req.body.name
        })
        return res.status(200).json({
            msg: 'discount deleted successfully'
        })
    }
    catch (e) {
        console.log(e)
        return res.status(400).json({
            error: 'an error occured'
        })
    }
}


// /discount/changestatus mehtod:post
async function changeStatusDiscount(req, res) {
    try {
        const errors = []
        var error

        //check if name is empty
        error = await isEmpty(req.body.name, 'name')
        if (error != undefined) {
            errors.push(error)
        }

        //handling errors
        if (errors.length != 0) {
            return res.status(400).json({
                errors: errors
            })
        }

        //check if a discount with this name is not exists
        const existDiscount = await Discount.findOne({ name: req.body.name })
        if (existDiscount == null) {
            return res.status(404).json({
                error: `'${req.body.name}' is not found`
            })
        }

        //if everything was ok!
        if (existDiscount.active == true) {
            await Discount.updateOne({
                name: req.body.name
            }, {
                active: false
            })
            return res.status(200).json({
                msg: 'Discount deactivated'
            })
        }
        else {
            await Discount.updateOne({
                name: req.body.name
            }, {
                active: true
            })
            return res.status(200).json({
                msg: 'Discount activated'
            })
        }
    }
    catch (e) {
        console.log(e)
        return res.status(400).json({
            error: 'an error occured'
        })
    }
}



module.exports = {
    createDiscount,
    showDiscounts,
    updateDiscount,
    deleteDiscount,
    changeStatusDiscount,
}