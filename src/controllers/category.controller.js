const Category = require('../models/product_category.model');
const { getPagination } = require('../services/query')

async function creatCategory(req, res) {
    //checking if name is null
    if (!req.body.name || !req.body.description) {
        return res.status(400).json({
            error: 'name and description field is required!'
        })
    }

    //checking if we have already a category with this name
    const existCategory = await Category.find({
        name: req.body.name
    },
        {
            name: 1,
            _id: 0,
        })
    if (existCategory.length != 0) {
        return res.status(400).json({
            error: 'category with this name already exists!'
        })
    }

    try {
        Category.create({
            name: req.body.name,
            description: req.body.description,
        })

        return res.status(200).json({
            msg: 'category created successfull!'
        })
    } catch (e) {
        return res.status(500).json({
            error: e
        })
    }
}


async function showCategories(req, res) {

    try {
        const { limit, skip } = getPagination(req.query)

        const categories = await Category.find({}, {
            _id: 0,
            __v: 0,
        })
            .limit(limit)
            .skip(skip)
            .sort({ created_at: -1 });

        return res.status(200).json(categories)
    }
    catch (error) {
        console.log(error);
        return res.status(400).json({
            error: 'an error occured!'
        })
    }

}



async function updateCategory(req, res) {
    try {

        //check if name or description is empty
        if (!req.body.name || !req.body.description) {
            return res.status(400).json({
                error: 'name and description field is required!'
            })
        }

        //check if a category with this name already exist
        const existCategory = await Category.findOne({
            name: req.body.name
        });

        if (existCategory != null && existCategory.id != req.body.id) {
            return res.status(400).json({
                error: 'category with this name already exists!'
            })
        }

        //when every thing is ok:
        await Category.updateOne({
            _id: req.body.id
        },
            {
                name: req.body.name,
                description: req.body.description,
                modified_at: Date.now()
            })

        return res.status(200).json({
            msg: 'updated successfully'
        })
    }
    catch (error) {
        console.log(error)
        return res.status(400).json({
            error: 'an error occured!'
        })
    }
}


async function deleteCategory(req, res) {
    try {
        //check if name is empty
        if(!req.body.name) {
            return res.status(400).json({
                error: 'name is empty!'
            })
        }

        //check if there is no category with this name
        const existCategory = await Category.findOne({
            name: req.body.name
        })
        if(existCategory == null) {
            return res.status(400).json({
                error: 'there is no category with this name'
            })
        }

        //if everything was ok
        await Category.deleteOne({
            name: req.body.name,
        })
        return res.status(200).json({
            msg: 'Category deleted successfully!'
        })
    }
    catch(error) {
        console.log(error)

        return res.status(400).json({
            error: 'an error occured!'
        })
    }
}

module.exports = {
    creatCategory,
    showCategories,
    updateCategory,
    deleteCategory
}