const jwt = require('jsonwebtoken');
require('dotenv').config();

//registering new user
function sign_up(req, res) {

    //token that we are going to send
    const token = jwt.sign({
        email: 'test@test.com',
        password: 123
    }, process.env.JWT_KEY,
    {
        expiresIn: '1h'
    },
    (err, token) => {
        if(err) {
            res.json(err);
            throw err;
        }
        return res.json(token);
    });

     
}


module.exports = {
    sign_up,
};