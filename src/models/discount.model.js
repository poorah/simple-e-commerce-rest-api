const mongoose = require('mongoose')

const discountSchema = new mongoose.Schema({

    name: {
        type: String,
        required: true,
        unique: true,
    },

    description: {
        type: String,
        required: true,
    },

    discount_percent: {
        type: Number,
        required: true
    },

    active: {
        type: Boolean,
        requied: true,
        default: false
    },

    created_at: {
        type: Date,
        required: true,
        default: Date.now(),
    },

    modified_at: {
        type: Date,
    }
})

module.exports = mongoose.model('Discount', discountSchema)