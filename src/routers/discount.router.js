const express = require('express')
const {
    createDiscount,
    showDiscounts,
    updateDiscount,
    deleteDiscount,
    changeStatusDiscount,
} = require('../controllers/discount.controller')



const discountRouter = express.Router()


//create a new discount
discountRouter.post('/create', createDiscount)

//show discounts
discountRouter.get('/show', showDiscounts)

//update a discount
discountRouter.put('/update', updateDiscount)

//delete a discount
discountRouter.delete('/delete', deleteDiscount)

//change active flag
discountRouter.post('/changestatus', changeStatusDiscount)


module.exports = discountRouter