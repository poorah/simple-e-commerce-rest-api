const express = require('express');
const indexController = require('../controllers/index.controller');

const indexRouter = express.Router();

//main 
indexRouter.get('/',
    indexController
);

module.exports = indexRouter;