const express = require('express');
const swaggerUi = require("swagger-ui-express");
const swaggerDoc = require('../services/swagger.json');

const swaggerRouter = express.Router();


swaggerRouter.use('/api-docs', swaggerUi.serve);
swaggerRouter.get('/api-docs', swaggerUi.setup(swaggerDoc));


module.exports = swaggerRouter;