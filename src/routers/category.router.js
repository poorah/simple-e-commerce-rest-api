const express = require('express');
const {
    creatCategory,
    showCategories,
    updateCategory,
    deleteCategory } = require('../controllers/category.controller')


const categoryRouter = express.Router()

// creating category
categoryRouter.post('/create', creatCategory)

//showing categories
categoryRouter.get('/show', showCategories)

//updating a category
categoryRouter.put('/update', updateCategory)

//deleting a category
categoryRouter.delete('/delete', deleteCategory)


module.exports = categoryRouter;