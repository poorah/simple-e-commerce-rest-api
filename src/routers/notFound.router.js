const express = require('express');

const notFoundRouter = express.Router();

//endpoints that are not exist
notFoundRouter.get('*',
    (req, res) => { //TODO: making controller
        res.status(404).json({
            error: "not found!"
        });
    });

module.exports = notFoundRouter;