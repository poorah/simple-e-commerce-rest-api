const cors = require('cors');
const morgan = require('morgan');
const express = require('express');
const rfs = require("rotating-file-stream");
const indexRouter = require('./routers/index.router');
const swagger = require('../src/routers/swagger.router');
const notFoundRouter = require('./routers/notFound.router');
const categoryRouter = require('./routers/category.router');
const discountRouter = require('./routers/discount.router')
const { sign_up } = require('./controllers/authentication.controller');


const app = express();

//using cors package for enabeling Cross-Origin Resource Sharing request
app.use(cors());


//parsing requests to JSON
app.use(express.json());


// MORGAN SETUP
// create a log stream
const rfsStream = rfs.createStream("log.txt", {
    size: '10M', // rotate every 10 MegaBytes written
    interval: '1d', // rotate daily
    compress: 'gzip' // compress rotated files
})

// add log stream to morgan to save logs in file
app.use(morgan("dev", {
    stream: rfsStream
}));

// another logger to show logs in console as well
app.use(morgan("dev"));


//index
app.use(indexRouter);


//404 not found
//app.use(notFoundRouter);


//sign_up
app.post('/auth', sign_up);

//swagger : DEVELOPING
app.use(swagger);

// category
app.use('/product-category', categoryRouter);

// disount
app.use('/discount', discountRouter)

module.exports = {
    app,
};