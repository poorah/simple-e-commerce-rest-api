const mongoose = require('mongoose');
require('dotenv').config();

//URL in .env file
const MONGO_URL = process.env.MONGO_URL;

//connecting to mongoDB function
async function connectToMongoDB() {

    await mongoose.connect(MONGO_URL)
        .catch(error => handleError(error))
        .then(() => {
            console.log('MONGO connected!');
        });

}
module.exports = {
    connectToMongoDB,
}